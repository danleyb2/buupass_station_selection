from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

# Create your views here.


places = {"1":"Central","2":"Coast","3":"Eastern","4":"Nairobi","5":"North","6":"Nyanza","7":"Rift","8":"Western"}
Central = ["Kiambu", "Nyeri", "Limuru", "Kinangop"]
Coast = ["Mombasa","Kilifi","Ukunda","Mtwapa","Malindi"]
Eastern = ["Kitui","Mwingi","Machakos","Kibwezi","Matuu"]
Nairobi = ["SouthC","Madaraka","Dagoretti","Kibera","Embakasi"]
North = ["Wajir","Mandera","Ijara","Isiolo"]
Nyanza = ["Kisumu","Kuria","Bondo","Kisii","Nyamira"]
Rift = ["Kericho","Bomet","TransNzoia","Eldoret","Naivasha","Nakuru"]
Western = ["Bungoma","Kakamega","Busia","Elgon"]

@csrf_exempt
def ussd(request):
    sessionId = request.POST.get('sessionID')
    serviceCode = request.POST.get('serviceCode')
    phoneNumber = request.POST.get('phoneNumber')
    text = request.POST.get('text')

    welcome = (
            "CON Welcome to BuuPass\n"
            "Select region:\n"
            "\n"
            "\t1.Central\n"
            "\t2.Coast\n"
            "\t3.Eastern\n"
            "\t4.Nairobi\n"
            "\t5.NorthEastern\n"
            "\t6.Nyanza\n"
            "\t7.RiftValley\n"
            "\t8.Western\n"
            "\t99.QUIT\n" 
    )
    
   # answer = (
    #        "CON Select Town:\n"
     #       "\t1.Kaloleni\n"
      #      "\t2.Mombasa\n"
       #     "\t3.Kilifi\n"
        #    "\t4.Malindi\n"
         #   "\t5.Ukunda\n"

            #"%s\n"%place
          #  "\t0.BACK\n"
           # "\t99.QUIT\n"
    #)
        
    
    if not text:
        response = welcome
    else:
        if text.isdigit():
            if len(text) == 1  :
                place = places[text]
                response = answer(place)
                #response = answer
            else:
                response = "END"

        else:
            response = welcome
           


    print('\n')
    print(response)
    print('\n')

    return HttpResponse(response,content_type='text/plain')
    
def answer(place):
    Answer = "Select town:\n\n"
    c = 1
    for i in place:
        Answer += "\t%d.%s\n"%(c,i)
        c+=1
    Answer += "\t0.BACK\n\t99.QUIT\n"
    return Answer   
